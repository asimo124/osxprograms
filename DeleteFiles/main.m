//
//  main.m
//  DeleteFiles
//
//  Created by alex hawley on 7/17/16.
//  Copyright © 2016 alex hawley. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSFileManager *fm = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        // Loop through documents folder
        NSString *directory = [documentsDirectory stringByAppendingPathComponent:@""];
        NSError *error = nil;
        for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
            
            // Check if file contains extension '.swiftk'
            NSMutableArray *myFiles = [[NSMutableArray alloc] init];
            [myFiles addObject:file];
            NSArray *returnFiles = [myFiles pathsMatchingExtensions:[NSArray arrayWithObjects:@"swiftk", nil]];

            if (returnFiles.count > 0) {  // if does contain .swiftk
                
                // build string
                NSMutableString *fullPath = [NSMutableString stringWithCapacity: 300];
                [fullPath setString: directory];
                [fullPath appendString:@"/"];
                [fullPath appendString:(file)];
                NSLog(@"fullPath: %@", fullPath);
                
                // delete
                BOOL success = [fm removeItemAtPath: fullPath error: &error];
                if (!success || error) {
                    NSLog(@"couldn't delete file");
                }
            }
        }
    }
    return 0;
}
